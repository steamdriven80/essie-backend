#!/bin/bash

numCodes=$1

echo "Printing $numCodes barcodes, which will take ~ $((numCodes*5 / 60)) minutes."
echo "Continue? "

read Answer

if [ "$Answer" = 'n' ] || [ "$Answer" = 'N' ]; then
    exit 0
fi

ID=0

for I in `seq 1 $numCodes`; do

	ID=$((ID+1))

	python2 ./tools.py

	rasterizer -m application/pdf scratch.svg -d BarcodePrinter_$ID.pdf

	lpr -o ppi=300 -o PageSize=w167h288 -o PrintQuality=Graphics BarcodePrinter_$ID.pdf

done
