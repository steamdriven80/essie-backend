var currentStage = 0;

var barcodeEl = document.getElementById("barcode");
var errEl = document.getElementById("error_label");

var c = document.getElementsByClassName("barwrap");

function updateStage() {
    currentStage = Math.min(currentStage, 1);

    console.log("currentStage: ", currentStage);

    if (currentStage > 0) {
        c[1].classList.remove('hider');
    } else {
        c[1].classList.add('hider');
    }
}


function displayData (item) {

    var detailsTemplate = document.getElementById("detailsTemplate").innerHTML;
    var detailsParent = document.getElementsByClassName("detailsParent")[0];
    var details = [];

    var el = detailsTemplate,
            snip = "";

    try {
        el = el.replace("%i.product_url%", item['product_url'] || 'http://placehold.it/250x250');
    } catch(e) {}

    try {
        el = el.replace("%i.barcode_url%", item['barcode_url']);
    } catch(e) {}

    // replace the templated variables
    for (var j = 0; j < window.Schema.length; j++) {

        var tag = window.Schema[j];


        el = el.replace("%i." + tag + "%", item[tag] || '');
    }

    // check if the item is 'available'

    if (item['checkout_user'] && item['checkout_user'].length) {
        console.log("ITEM IS CHECKED OUT - THIS WOULD CHECK IT BACK IN");
    } else {
        console.log("ITEM IS CHECKED IN - THIS WOULD CHECK IT OUT");
    }

    detailsParent.innerHTML = el;

}


function grabBarcode (barcode) {

    if ( !barcode ) {
        barcode = barcodeEl.value;
    }


    var errorReset = true;


    $.ajax( {
        url: "http://localhost:5000/items/search",
        type: "GET",
        data: {
            "query": "barcode="+barcode
        },
        complete: function(response) {

            console.log("DATA: ",response);

            if (response.status !== 200) {
                console.warn("Unable to contact server.  Please try again another time.");
                return;
            }

            var r = JSON.parse(response.responseText);

            if (r.count == 0) {
                console.log("Query returned 0 results.");

                currentStage = 0;
                updateStage();

                if (!errorReset) { return; }



                errEl.innerHTML = "Unable to find any objects that match.";

                errEl.classList.remove('slidOffscreen');

                setTimeout((function(errEl) { return function() { errEl.classList.add('slidOffscreen'); errorReset = true; }; })(errEl), 3000);

                return;
            }
            else if (r.count > 1 ) {
                console.log("Query returned more than one object...  Choosing one of many.");

                r.count = 1;
            }

            var item = r.results[0];

            currentStage ++;

            updateStage();

            displayData(item);

            window.Item = item;

            $("#barcode").blur();
            $("#inout").focus();
        }
    });
}

var checkForEnter = function(e) {
    console.log( "KEYDOWN : ", e );

    if (e.keyCode == 13) {
        grabBarcode(barcodeEl.value);

        barcodeEl.value = "";

        return false;
    }
};

barcodeEl.onkeydown = checkForEnter;
barcodeEl.onsubmit = grabBarcode;

window.Schema = [
    "essie_barcode",
    "uf_inventory",
    "type",
    "type2",
    "category",
    "manufacturer",
    "model",
    "description",
    "serial",
    "location",
    "gps",
    "id",
    "last_check",
    "calibration_doc",
    "next_check",
    "range_units",
    "range_min",
    "range_max",
    "excitation",
    "interface",
    "measure_units",
    "measure_min",
    "measure_max",
    "room",
    "manual",
    "oco_decal",
    "source_grant",
    "created_on",
    "checkout_time",
    "checkout_user",
    "product_url",
    "barcode"
];

document.querySelector("#barcode").focus();

function checkinout() {
    console.log('checking....');

    $("#inout").visible = false;

    $.ajax({
        url: '/items/inout',
        type: 'POST',
        data: {
            'which': window.Item['id']
        },
        complete: function(resp) {
            console.log("RESPONSE: ", resp);

            var errEl = document.getElementById('error_label');

            if ( resp.status == 200 && resp.responseText.indexOf('check-in') !== -1 ) {
                errEl.innerHTML = "Item Successfully Checked In";
            }
            else if ( resp.status == 200 ) {
                errEl.innerHTML = "Item Successfully Checked Out";
            }
            else {
                errEl.innerHTML = "An Unknown Error Occurred";
            }

            errEl.classList.remove('slidOffscreen');

            $("#barcode").focus();

            setTimeout((function(errEl) { return function() { errEl.classList.add('slidOffscreen'); errorReset = true; }; })(errEl), 3000);

            setTimeout(function () {
                document.getElementsByClassName("detailsParent")[0].innerHTML = "";
                $("#inout").visible = true;
                $("#barcode").focus();
            }, 3000);
        }
    });
}