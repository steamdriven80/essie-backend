#!/usr/bin/env python

import csv
import hashlib
import random
import logging
import os

import pymongo as mongo
import barcode
import config
import time


# host = config.get('mongodb')['host']
# port = config.get('mongodb')['port']
# database = config.get('mongodb')['database']
# _username = config.get('mongodb')['username']
# _password = config.get('mongodb')['password']
#
# _append_uri = config.get('mongodb')['append_uri']
#
# full_url = 'mongodb://' + _username + ':' + _password + '@' + host + ':' + port
#
# if _append_uri:
#     full_url = full_url + '/' + database
#
# client = mongo.MongoClient(full_url)

#
# db = client.essie['items']
# users = client.essie['users']
# hipergator = client.essie['hipergator']

db = []

def load_initial_db():

    csv_files = ['vision', 'sensing', 'control', 'data']

    path = os.path.join( os.path.dirname(__file__), 'db-source')

    for cat in csv_files:

        data = _load_csv(os.path.join(path,cat+'.csv'), cat)

        print "Loaded %d records from %s" % (data['row_count'], cat+'.csv')

        db.extend(data['rows'])


def _load_csv(filename, category=None):

    data = list(csv.DictReader(open(filename, 'rb')))

    if len(data) == 0:
        print "[warning]: CSV file %s empty or not found. no data loaded." % os.path.basename(filename)

    document = []

    for i in xrange(len(data)):
        data[i]['id'] = hashlib.md5(str(data[i])).hexdigest()
        data[i]['essie_barcode'] = random.randint(10000000, 99999999)
        data[i]['category'] = category

        document.append(data[i])

    item_count = len(document)

    return {'row_count': item_count, 'rows': document}

UniqueValue = random.randint(10000000, 99999999)

def load_value():

    global UniqueValue

    with open("unique.opt", "rt") as unique:

        Dat = 0

        Dat = unique.readline()

        UniqueValue = int(Dat)

    print "Read Value of %d" % (UniqueValue)

def save_value():

    global UniqueValue

    with open("unique.opt", "wt") as unique:

        print >> unique, str(UniqueValue)

    print "Wrote Value of %d" % (UniqueValue)

def generate_1x_barcode():

    global UniqueValue

    load_value()

    UniqueValue += 1

    print 'Atomic unique value: %d' % UniqueValue

    save_value()

    value = str(UniqueValue)

    value = value + ('0' * (13-len(value)))

    print 'barcode value is: %s' % value

    where = barcode.get('ean13', value).save('scratch')

    html = ''.join(open(where, "rt").readlines())

    print "READ IN SVG VALUE: \n"

    print html

    return html


def generate_Nx_barcodes( NumBarcodes ):

    HTMLDoc = """
    <html>
    <head>
        <style>


        </style>
    </head>
    <body>
    """

    for i in xrange(0, NumBarcodes):

        value = generate_1x_barcode()

        HTMLDoc += "<div class='barcode'>" + value + '</div>'

    HTMLDoc += "</body></html>"


    with open("barcodes.%d.html","wt") as viewer:

        viewer.writelines(HTMLDoc)


def save_product_image(id, file):

    if not os.path.exists('static/img/'):
        os.mkdir('static/img')

    if not os.path.exists('static/img/product/'):
        os.mkdir('static/img/product')

    url = ''

    try:
        if file.content_type == 'image/gif':
            url = str('id_' + id +'.gif')
            if len(url) <= 4:
                raise RuntimeError('Invalid Product ID')

            with open('static/img/product/' + url, 'wb') as out:
                for c in file.chunks():
                    out.write(c)

            url = '/static/img/product/'+url
        elif file.content_type == 'image/jpeg':
            url = str('id_' + id +'.jpg')
            if len(url) <= 4:
                raise RuntimeError('Invalid Product ID')

            with open('static/img/product/' + url, 'wb') as out:
                for c in file.chunks():
                    out.write(c)

            url = '/static/img/product/'+url
    except:

        return ""

    return url


if __name__ == '__main__':

    logging.info("Generating barcode.")

    print >> open("barcode.html","wt"), generate_1x_barcode()

    # logging.info("Admin Toolbox v0.2")
    # logging.info("Loading initial database from file.")
    #
    # load_initial_db()
    #
    # time.sleep(1)
    #
    # logging.info("Load Complete.")
    #
    # logging.info("Generating all barcodes.")
    #
    # time.sleep(1)
    #
    # generate_all_barcodes()
    #
    # logging.info("Generating Complete.")
    #
    # time.sleep(2)