__author__ = 'chris'

import 	inspect, logging, chalk

def __init__():

    logger = logging.getLogger(__name__)

    handler = chalk.log.ChalkHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

def info( message ):
    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    try:
        # do something with the frame
        logging.info(chalk.magenta("["+mod.__name__ + "]:", opts=['bold']), " -> ", chalk.blue(message, opts=['bold']))
    finally:
        del frm, mod


def warn( message ):
    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    try:
        # do something with the frame
        logging.warn((chalk.magenta("["+mod.__name__ + "]:", opts=['bold'])), " -> ", chalk.yellow(message, opts=['bold']))
    finally:
        del frm, mod


def error( message ):
    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    try:
        # do something with the frame
        logging.error(chalk.magenta("["+mod.__name__ + "]:", opts=['bold']), " -> ", chalk.red(message, opts=['bold']))
    finally:
        del frm, mod


def success( message ):
    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    try:
        # do something with the frame
                logging.log(logging.DEBUG, chalk.magenta("["+mod.__name__ + "]:", opts=['bold']), " -> ",chalk.green(message, opts=['bold']))
    finally:
        del frm, mod
