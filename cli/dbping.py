#!/usr/bin/env python2

# __author__ = 'chris'

import pymongo
import cli.cmd
from cli.helpers import info, warn, error, success

import sitecfg

class dbping(cli.cmd.Command):

    dbhost = sitecfg.get('dbhost')
    dbname = sitecfg.get('dbname')
    collection = sitecfg.get('dbtable')

    full_uri = "mongodb://"+sitecfg.get("dbuser")+":"+sitecfg.get("dbpass")+"@"+dbhost

    @classmethod
    def dependencies(cls):

        return ["pymongo"]

    @classmethod
    def load(cls):

        return True

    @classmethod
    def run(cls, *args, **kwargs):

        info( "Ping..." )

        try:
            cls.view = pymongo.MongoClient(cls.full_uri)

            cls.view = pymongo.MongoClient(cls.full_uri)

            success( "Pong!" )

        except Exception,e:
            error( "Failed." )

            raise e

        return True

def register(mgr):
    mgr.add("dbping", dbping)