class Command:

    def __init__(self):
        pass

    def dependencies(self):
        """
        a child object should overload this method and return a list
        of other modules it is dependent on
        :return:    a list of other modules that must be loaded first,
                    or an empty list if None
        """

        return []

    def load(self):
        """
        called the first time a command is loaded/referenced and only called one
        :return: True if command is ready to go, False to remove command from pool
        """

        return True

    def run(self, *arg, **kw):
        """
        The main execution method, this method is invoked both from the command line
        and from other commands.  All postional arguments will be in *arg, and all
        dictionary key=value arguments in **kw
        :return:    a value specific to the method being run
        """

        return True

    def unload(self):
        """
        called when the programs execution has ended and all modules are being unloaded.
        only called once and will never be called again.
        :return:    a tuple with True/False for the first value and an error message as the second or None
                        e.g. (True, None)       ;       (False, "Memory Error!  Permission denied.")
        """

        return (True, None)