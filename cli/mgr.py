__author__ = 'chris'

from helpers import *
from cmd import Command
import importlib

class CmdMgr:

    ready = {}
    broken = {}

    @classmethod
    def add(cls, title, cmd):
        """
        called by a command to register itself for loading and use.
                e.g.     mgr.add("ping", PingCmd) <- 'class PingCmd(cli.cmd)'
        :param title:    a string used to reference this command. must be globally unique
        :param cmd:      the command object that presents a test.cmd Interface
        """

        if title in cls.ready:       # modules already been loaded

            if cmd != cls.ready[title]:

                warn("Overwriting a previous command '%s' with a new, different implementation." % title)

                cls.ready[title] = cmd

            return

        if title in cls.broken:
            del( cls.broken[title] )

        if not cls.check_dependencies(cmd):
            cls.broken[title] = cmd
        else:
            cls.ready[title] = cmd


    @classmethod
    def check_dependencies(cls, cmd):

        depends = cmd.dependencies()

        for module in depends:
            try:
                importlib.import_module(module)
            except ImportError, e:
                return False
            except ImportWarning, e:
                warn(e.message)

        return True