#!/bin/bash

# This build helper script uses the NSIS library to generate a fully
# encapsulated Windows installer, including Python 2.7.6 and all required
# libraries.

pynsist platform/windows/installer.cfg
