import datetime
import uuid

import pymongo as mongo

import config


import os, csv, random

class BackendDB:

    # host = config.get('mongodb')['host']
    # port = config.get('mongodb')['port']
    # database = config.get('mongodb')['database']
    # _username = config.get('mongodb')['username']
    # _password = config.get('mongodb')['password']
    #
    # _append_uri = config.get('mongodb')['append_uri']
    #
    # full_url = 'mongodb://' + _username + ':' + _password + '@' + host + ':' + port
    #
    # if _append_uri:
    #     full_url = full_url + '/' + database
    #
    # client = mongo.MongoClient(full_url)
    #
    # items = {'db': client.essie.items}
    # items['cache'] = list(items['db'].find({}))
    # users = {'db': client.essie.users}
    # hipergator = {'db': client.essie.hipergator}

    items = []

    unique = random.randint(10000000, 99999999)

    @classmethod
    def load_initial_db(cls):

        csv_files = ['vision', 'sensing', 'control', 'data']

        path = os.path.join( os.path.dirname(__file__), 'db-source')

        for cat in csv_files:

            data = BackendDB._load_csv(os.path.join(path,cat+'.csv'), cat)

            print "Loaded %d records from %s" % (data['row_count'], cat+'.csv')

            BackendDB.items.extend(data['rows'])

    @classmethod
    def _load_csv(cls, filename, category=None):

        data = list(csv.DictReader(open(filename, 'rb')))

        if len(data) == 0:
            print "[warning]: CSV file %s empty or not found. no data loaded." % os.path.basename(filename)

        document = []

        global unique

        for i in xrange(len(data)):
            data[i]['id'] = unique + 1
            unique = unique + 1
            data[i]['essie_barcode'] = -1
            data[i]['category'] = category

            document.append(data[i])

        item_count = len(document)

        return {'row_count': item_count, 'rows': document}


    @classmethod
    def check_cache(cls, set):

        if len(cls.items) == 0:
            cls.load_initial_db()

        set['cache'] = cls.items

        # if set['cache'] is None or force or datetime.datetime.now() > set.get('cache-expires', datetime.datetime.now().replace(hour=0)):
        #     set['cache-expires'] = datetime.datetime.now() + datetime.timedelta(minutes=5)
        #     set['cache'] = list(set['db'].find({}))

    @classmethod
    def save_to_disk(cls):

        with open("db-source/raw.db", "wt") as raw:

            global items

            raw.writelines(items)

        print "DB Synced to Disk"

    @classmethod
    def load_from_disk(cls):

        with open("db-source/raw.db", "rt") as raw:

            global items

            items = raw.readlines()

        print "Read from disk"

    @classmethod
    def findItemByID(cls, ID):

        cls.check_cache(cls.items)

        for i in cls.items['cache']:
            if i['id'] == ID:
                return i

    @classmethod
    def getAllItems(cls):

        cls.check_cache(cls.items)

        try:
            return cls.items['cache']
        except:
            return None

    @classmethod
    def getAllItemsByPage(cls, offset, limit):

        cls.check_cache(cls.items)

        try:
            return cls.items['cache'][offset:offset+limit]
        except:
            return cls.items['cache'][-limit:]

    @classmethod
    def getAllItemsCount(cls):

        cls.check_cache(cls.items)

        return len(cls.items['cache'])

    @classmethod
    def findItemByDict(cls, criteria):
        return list(cls.items['db'].find(criteria))


    @classmethod
    def updateItem(cls, criteria, values):

        ret = cls.items['db'].update(criteria, {'$set': values})

        cls.check_cache(cls.items, True)

        return ret

    @classmethod
    def insertItem(cls, item):

        ret = cls.items['db'].insert(item)

        cls.check_cache(cls.items, True)

        return ret


_cols = {
         "Calibration Doc": "" ,
        "Calibration Expires": "" ,
        "Category": "" ,
        "Description": "" ,
        "Excitation (V)": "" ,
        "Interface Protocol": "" ,
        "ImageUrl": "",
        "Last Calibrated": "" ,
        "Location": "" ,
        "Manual": "" ,
        "Manufacturer": "" ,
        "Model": "" ,
        "OCO Decal No.": "" ,
        "Output Max": "" ,
        "Output Min": "" ,
        "Output Units": "" ,
        "Range Max": "" ,
        "Range Min": "" ,
        "Range Units": "" ,
        "Room": "" ,
        "Serial Number": "" ,
        "Source Grant": "" ,
        "Type 1": "" ,
        "Type 2": "" ,
        "UF Inventory Number": "",
        "QRUrl": "",
        "Status": "",
        "UPCUrl": "",
        "LastSeen": ""
    }

_schema = {
    'version': '0.0.1',
    'fields': {
        'essieqr': {'displayName': 'ESSIE QR Code', 'summary': True, 'style': 'width:50px;height:50px;',
                    'hide': False, 'description': 'The ESSIE QR image and the plaintext data encoded in it'},  #
        'uf_inventory': {'displayName': 'UF Inventory Number', 'summary': False, 'style': 'width:10%;',
                         'hide': False, 'description': 'University of Florida assigned inventory ID'},  #
        'type': {'displayName': 'Type', 'summary': False, 'style': 'width:10%;', 'hide': False,
                 'description': 'One of four broad categories of ESSIE inventory'},
        'manufacturer': {'displayName': 'Manufacturer', 'summary': False, 'style': 'width:10%;', 'hide': False,
                         'description': 'The items parent company, or a representative brand'},
        'model': {'displayName': 'Model', 'summary': False, 'style': 'width:10%;', 'hide': False,
                  'description': 'The product line or version'},  #
        'description': {'displayName': 'description', 'summary': False, 'style': 'width:10%', 'hide': False,
                        'description': 'A short description of the item from the vendors marketing material'},
        # Approx a paragraph describing the item
        'serial': {'displayName': 'Serial Number', 'summary': False, 'style': 'width:10%', 'hide': False,
                   'description': 'Any unique serial the item might have from the manufacturer'},  #
        'location': {'displayName': 'Location', 'summary': False, 'style': 'width:10%', 'hide': False,
                     'description': 'The main storage facility of this item.'},
        'gps': {'displayName': 'Last GPS', 'summary': True, 'style': 'width:10%;', 'hide': False,
                'description': 'The GPS coordinates of the previous location this item was scanned'},
        'lastcheck': {'displayName': 'Last Calibration', 'summary': False, 'style': 'width:10%;', 'hide': False,
                      'description': 'The most recent instrument calibration, if applicable'},
        'nextcheck': {'displayName': 'Next Calibration', 'summary': False, 'style': 'width:10%;', 'hide': False,
                      'description': 'The next scheduled instrument calibration, if applicable'},
        'rangeUnits': {'displayName': 'Units of Range', 'summary': True, 'style': 'width:10%;', 'hide': True,
                       'description': 'The units measured throughout the items range'},
        'rangeMin': {'displayName': 'Range Min', 'summary': True, 'style': 'width:10%;', 'hide': False,
                     'description': 'The minimum value expected to not cause permanent damage'},
        'rangeMax': {'displayName': 'Range Max', 'summary': True, 'style': 'width:10%;', 'hide': False,
                     'description': 'The maximum value expected to not cause permanent damage'},
        'measureUnits': {'displayName': 'Units of Measurement', 'summary': True, 'style': 'width:10%;',
                         'hide': True, 'description': 'The units used by the instrument for measurements'},
        'measureMin': {'displayName': 'Min Measurement', 'summary': True, 'style': 'width:10%;', 'hide': False,
                       'description': 'The minimum value expected to provide repeatable, reliable results'},
        'measureMax': {'displayName': 'Max Measurement', 'summary': True, 'style': 'width:10%;', 'hide': False,
                       'description': 'The maximum value expected to provide repeatable, reliable results'},
        'room': {'displayName': 'Room', 'summary': False, 'style': 'width:10%', 'hide': False,
                 'description': 'The room number where this item is typically stored'},
        'manual': {'displayName': 'Manual', 'summary': False, 'style': 'width:10%', 'hide': False,
                   'description': 'A link to a local digital copy of the product manual, if available'},
        'ocodecal': {'displayName': 'OCO Decal No.', 'summary': False, 'style': 'width:10%;', 'hide': False,
                     'description': 'Another external tracking decal (?)'},
        'sourcegrant': {'displayName': 'Source Grant', 'summary': False, 'style': 'width:10%', 'hide': False,
                        'description': 'The grant funding the purchase and maintenance of this item'}
        }
    }

class Item:

    
    def __init__(self):
        self._id = uuid.uuid4()
        self.essie_barcode = None
        self.uf_inventory = None
        self.type = None
        self.type2 = None
        self.category = None
        self.manufacturer = None
        self.model = None
        self.description = None
        self.serial = None
        self.location = None
        self.gps = None
        self.last_check = None
        self.calibration_doc = None
        self.next_check = None
        self.range_units = None
        self.range_min = None
        self.range_max = None
        self.excitation = None
        self.interface = None
        self.measure_units = None
        self.measure_min = None
        self.measure_max = None
        self.room = None
        self.manual = None
        self.oco_decal = None
        self.source_grant = None
        self.created_on = None
        self.checkout_time = None
        self.checkout_user = None
        self.checkout_user = None