import urllib2
import base64

import config


def authenticate(username, password, hpc = False):
    """
    This function validates a username and password against the UF Active Directory system.
    :param username: a string with the user's Gatorlink username
    :param password: a string with the user's password
    :param hpc: a boolean value with True validating against Research Computing Accounts and False normal Gatorlink
    :return:    True if user is valid, False if not or None if another error occurred
    """

    if hpc is True:
        url = config.get('authentication')['research_host']
    else:
        url = config.get('authentication')['normal_host']

    request = urllib2.Request(url)

    # we purposefully don't store the password
    request.add_header('Authorization', "Basic %s" % base64.encodestring('%s:%s' % (username, password)).replace('\n', ''))

    try:
        result = urllib2.urlopen(request)

    except urllib2.HTTPError, e:
        return False

    return True