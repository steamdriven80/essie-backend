map = { 'dashboard': '/',
        'inventory': {'view':'/items/',
                      'checkout':'/items/inout',
                      'manuals':'/items/manuals'},
        'hpc': {'news':'/hpc/news',
                    'submitjob':'/hpc/submitjob',
                    'viewtask':'/hpc/viewtask',
                    'developer':'/hpc/developer',
                    'support':'/hpc/support'}}
