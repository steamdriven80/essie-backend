import json
import os
import logging

_cfg = {}
_file = 'secret.json'
_loaded = None
_dirty = False

def get(item_name):
    """
    :param item_name: the key name to get
    :return: the value of key or 'None' if no such key
    """


    global _cfg, _loaded

    if not _loaded:
        _load_config()

    return _cfg.get(item_name)


def set(item_name, value, sync_asap = True):
    """
    :param item_name: the key name to set
    :param value: the value to set the key to
    :param sync_asap: write to db before returning? (default: True)
    :return: the 'value' parameter passed in is returned.  Errors are
             indicated using the raise...except pattern
    """

    global _cfg, _loaded, _dirty

    if not _loaded:
        _load_config()

    _cfg[item_name] = value

    if sync_asap:
        _save_config()
    else:
        _dirty = True

    return value


def sync(toDisk = True):
    """
    Called to ensure the copy of data in memory matches that on disk
    :param toDisk: True saves the copy of memory to disk, False reads it the other way
    :return:
    """

    global _dirty

    if toDisk and _dirty:

        if not _loaded:

            logging.warning("[config] -> saving a configuration to disk without first loading one.")

        _save_config()

    elif not toDisk:

        _load_config()


def _load_config():
    """
    Loads the configuration file into memory.
    :return:
    """

    global _loaded, _cfg


    if not os.path.exists("secret.json"):

        logging.fatal("[config] -> 'secret.json' configuration file not found!  "
                      "please check the file exists and restart the program.")

        raise IOError


    with open("secret.json", "rt") as input:

        _cfg = json.load(input)

        _loaded = True


def _save_config():
    """
    Saves the configuration file to disk.
    :return:
    """

    global _cfg, _loaded


    with open("secret.json", "wt") as output:

        json.dump(_cfg, output)

        _dirty = False