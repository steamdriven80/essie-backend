#!/usr/bin/env python2

import nose, os, logging

def runTestPkg( pkgName, stopOnError = True ):

    fullPath = os.path.join( os.path.join(os.path.join(os.path.dirname__file__), "tests"), pkgName)

    if not os.path.exists(fullPath):
        logging.warn("Attempted to run invisible test pkg '%s'!  We can't find them!\n \t'%s' ?" % (pkgName, fullPath))
        return False

    # The arguments to `run()` are the same as to `main()`:
    #
    # * module: All tests are in this module (default: None)
    # * defaultTest: Tests to load (default: '.')
    # * argv: Command line arguments (default: None; sys.argv is read)
    # * testRunner: Test runner instance (default: None)
    # * testLoader: Test loader instance (default: None)
    # * env: Environment; ignored if config is provided (default: None;
    #   os.environ is read)
    # * config: :class:`nose.config.Config` instance (default: None)
    # * suite: Suite or list of tests to run (default: None). Passing a
    #   suite or lists of tests will bypass all test discovery and
    #   loading. *ALSO NOTE* that if you pass a unittest.TestSuite
    #   instance as the suite, context fixtures at the class, module and
    #   package level will not be used, and many plugin hooks will not
    #   be called. If you want normal nose behavior, either pass a list
    #   of tests, or a fully-configured :class:`nose.suite.ContextSuite`.
    # * plugins: List of plugins to use; ignored if config is provided
    #   (default: load plugins with DefaultPluginManager)
    # * addplugins: List of **extra** plugins to use. Pass a list of plugin
    #   instances in this argument to make custom plugins available while
    #   still using the DefaultPluginManager.
    #
    # With the exception that the ``exit`` argument is always set
    # to False.

    return nose.run( module=pkgName )


def run_code_tests_and_coverage():

    pass
