import os.path
from whoosh.index import create_in
from whoosh.fields import *

from whoosh.query import *
from whoosh.qparser import QueryParser

ix = None


def is_init():

    global ix

    return ix is not None


def init_search(documents):

    global ix, _docs

    schema = Schema(id=STORED, barcode=TEXT, model=KEYWORD, manufacturer=KEYWORD, description=TEXT)

    if not os.path.exists('search_index'):
        os.mkdir('search_index')

    ix = create_in('search_index', schema)

    writer = ix.writer()

    for item in documents:
        value = []
        for k in list(str(item['id'])):
            if k in '0123456789':
                value.append(k)
        value = ''.join(value)

        writer.add_document(id=item['_id'], barcode=unicode(str(value)[0:12]), model=unicode(item['model']), manufacturer=unicode(item['manufacturer']), description=unicode(item['description']))

    writer.commit()

    return ix


def search(query, field="manufacturer"):

    global ix

    if ix is None:
        raise RuntimeError("[search] -> Attempting to Search Before Initiating System")

    if query is None:
        return

    parser = QueryParser(field, ix.schema)
    myquery = parser.parse(unicode(query))

    res = []

    with ix.searcher() as searcher:

        results = searcher.search(myquery, limit=None)

        for i in results:
            d = {}
            for k,v in i:
                d[k] = v

            res.append(i.docnum)

        return {'count': len(res), 'results': res }

    #return results