#!/usr/bin/env python

from functools import wraps
from datetime import datetime
import json
import re

from flask import request, Response, redirect, render_template, Flask, abort, session

from authentication import authenticate
import sitemap
import config
import database
import search




# If Cross-Origin Requests are desired, uncomment the lines marked CORS:

#CORS: from flask.ext.cors import CORS, cross_origin

app = Flask(__name__)

#CORS: cors = CORS(app)
#CORS: app.config['CORS_HEADERS'] = 'Content-Type'


if config.get('general')['environment'] == 'debug':
    app.debug = True


# set the secret key.  keep this really secret:
app.secret_key = config.get('general')['session_key']


# configure production mode to send errors as emails
if not app.debug:
    import logging
    from logging import Formatter
    from logging.handlers import SMTPHandler

    log_cfg = config.get('logging')

    mail_handler = SMTPHandler(mailhost=log_cfg['smtp_host'],
                               fromaddr=log_cfg['smtp_from'],
                               toaddrs=log_cfg['smtp_tos'],
                               credentials=(log_cfg['smtp_user'], log_cfg['smtp_pass']),
                               subject='ESSIE Inventory ERROR')
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(Formatter('''
        Message type:       %(levelname)s
        Location:           %(pathname)s:%(lineno)d
        Module:             %(module)s
        Function:           %(funcName)s
        Time:               %(asctime)s

        Message:

        %(message)s
        '''))
    app.logger.addHandler(mail_handler)



def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """

    normal_user = authenticate(username, password)
    admin_user = False

    if not normal_user:
        admin_user = authenticate(username, password, True)

    if normal_user or admin_user:
        session['user'] = {'username': username, 'password': password, 'authenticated': normal_user or admin_user, 'superuser': admin_user}
    else:
        session['user'] = None

    return session['user']


def unauthorized():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if auth:
            check_auth(auth.username, auth.password)

        if session.get('user', None) is not None:
            return f(*args, **kwargs)
        else:
            return redirect('/login?next=%s' % request.url.replace(request.host_url, '/'))
    return decorated


def administrator(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if auth:
            check_auth(auth.username, auth.password)

        user = session.get('user', None)
        if user and user['superuser']:
            return f(*args, **kwargs)
        elif not user:
            return redirect('/login?next=%s' % request.url.replace(request.host_url, '/'))
        else:
            return redirect(request.url)
    return decorated


@app.route('/', methods=['GET','POST'])
@requires_auth
def index():
    return redirect('/items')


@app.route('/login', methods=['POST','GET'])
def login_page():

    auth = request.authorization
    if auth:
        check_auth(auth.username, auth.password)

    if request.form.get('next') is not None:
        next = request.form['next']
    elif request.args.get('next') is not None:
        next = request.args['next']
    else:
        next = '/'

    if session.get('user', None) is not None:
        return redirect(next)


    if request.method == 'GET':
        return render_template('login.html', next=next)

    try:
        username = request.form['username']
        password = request.form['password']
    except:
        return unauthorized()


    user = check_auth(username, password)
    if user is not None:
        return redirect(next)
    else:
        # the authentication system was unable to verify the username and password
        return redirect('/login')


@app.route('/items/', methods=['GET','POST'])
@requires_auth
def item_list():
    try:
        offset = int(request.args['offset'])
        limit  = int(request.args['limit'])
    except:
        offset = 0
        limit = 10

    # Get all posts from DB
    try:
        posts = database.BackendDB.getAllItemsByPage(offset, limit)
    except:
        posts = []
        offset = 0
        limit = 10

    total_count = database.BackendDB.getAllItemsCount()

    if offset > 0:
        prev_offset = max( offset - limit, 0 )
        prev_limit = limit
    else:
        prev_offset = 0
        prev_limit = limit

    if offset+limit < total_count:
        next_offset = offset+limit
        next_limit = min(limit, total_count-(offset+limit))
    else:
        next_offset = offset
        next_limit = min(limit, total_count-offset)


    if prev_offset != offset:
        prevPage = '/items?offset=%d&&limit=%d' % (prev_offset, prev_limit)
    else:
        prevPage = ''

    if next_offset != offset:
        nextPage = '/items?offset=%d&&limit=%d' % (next_offset, next_limit)
    else:
        nextPage = ''

    return render_template('items_list.html', Items=posts, prevPage=prevPage, nextPage=nextPage, offset=offset, limit=limit, total_count=total_count, urls=sitemap.map, active='inventory.view')


@app.route('/items/search', methods=['POST','GET'])
#CORS: @cross_origin()
def search_json():

    if not search.is_init():
        search.init_search(database.BackendDB.getAllItems())

    response_data = {}

    finder = re.compile(r'([a-zA-Z0-9]+)\ ?(?::|=){1}\ ?([a-zA-Z0-9*\ ]+)')

    try:
        if request.method == 'POST':
            query = request.args['query']
            m = finder.search(query)
        else:
            query = request.args['query']
            m = finder.search(query)
    except Exception, e:
        query = ''
        return json.dumps({ 'query': query, 'results': [], 'status': 200 })

    try:
        where = m.group(1)
        query = m.group(2)
    except:
        where = 'model'

    results = search.search(query, where)


    response_data['results'] = []

    for x in results['results']:
        item = database.BackendDB.getAllItemsByPage(x,1)

        d = dict(item[0])
        n = {}
        for k in d:
            n[str(k)] = str(d[k])

        n['barcode_url'] = '/static/img/barcodes/'+n['id']+'.svg'
        value = []
        for k in n['id']:
            if k in '0123456789':
                value.append(k)
        value = ''.join(value)

        n['barcode'] = str(value)[0:12]

        response_data['results'].extend((n,))

    response_data['results'] = response_data['results']
    response_data['query'] = query
    response_data['count'] = results['count']
    response_data['status'] = 200
    response_data['where'] = where

    return json.dumps(response_data)


@app.route('/items/update', methods=['POST','GET'])
@administrator
def update():
    which = request.args['id']
    post = database.BackendDB.findItemByID(which)

    if not post or not len(post):
        abort(400)

    if request.method == 'POST':
        # update field values and save to mongo
        post['model'] = request.form['model']
        post['manufacturer'] = request.form['manufacturer']
        post['description'] = request.form['description']

        post['uf_inventory'] = request.form['uf_inventory']
        post['type'] = request.form['type']
        post['type2'] = request.form['type2']
        post['category'] = request.form['category']
        post['serial'] = request.form['serial']
        post['location'] = request.form['location']
        post['gps'] = request.form['gps']
        post['last_check'] = request.form['last_check']
        post['calibration_doc'] = request.form['calibration_doc']
        post['next_check'] = request.form['next_check']
        post['range_units'] = request.form['range_units']
        post['range_min'] = request.form['range_min']
        post['range_max'] = request.form['range_max']
        post['excitation'] = request.form['excitation']
        post['interface'] = request.form['interface']
        post['measure_units'] = request.form['measure_units']
        post['measure_min'] = request.form['measure_min']
        post['measure_max'] = request.form['measure_max']
        post['room'] = request.form['room']
        post['manual'] = request.form['manual']
        post['oco_decal'] = request.form['oco_decal']
        post['source_grant'] = request.form['source_grant']
        post['created_on'] = request.form['created_on']
        post['checkout_time'] = request.form['checkout_time']
        post['checkout_user'] = request.form['checkout_user']
        post['product_url'] = request.form['product_url']

        database.BackendDB.updateItem({'_id':which}, post)

        return redirect('/items/')

    elif request.method == 'GET':

        return render_template('items_update.html', item=post, urls=sitemap.map, active='inventory.view')


@app.route('/items/upload', methods=['GET','POST'])
@administrator
def upload_product_image():

    if request.method == 'POST':
        try:
            orig_id = request.headers['HTTP_ITEM_ID']
            f = request.files['the_file']
        except:
            orig_id = ''

        if len(orig_id) > 0 and len(f) > 0:
            # TODO: url = images.save_product_image(id, f)
            url = ''
            if len(orig_id) > 0 and len(url) > 4:
                database.BackendDB.updateItem({'id':orig_id},{'product_url': url})

            return json.dumps({'status':200, 'message': url}), 200, 'application/json'

    return json.dumps({'status':401, 'message': 'Invalid Request'})


@app.route('/logout')
def logout_page():

    # TODO: for debug purposes only, we disable POST request limiting
    # post requests use a CSRF token, this prevents cross site deauthentication requests
    #if not request.method == 'POST':
     #   return redirect('/')

    try:
        session.pop('user')
    except:
        pass
    try:
        session.clear()
    except:
        pass

    return redirect('/')


# @administrator
# @app.route('/items/delete', methods=['GET', 'POST'])
# def delete():
#     which = eval("request." + request.method + "['id']")
#
#     template = params = ''
#
#     if request.method == 'POST':
#         database.BackendDB.deleteItem(which)
#         template = 'snippet/_details.html'
#         params = {'Items': database.BackendDB.getItemsByPage(0, 10), 'urls': Urls(active='inventory.view')}
#     elif request.method == 'GET':
#         template = 'delete.html'
#         params = { 'id': id, 'urls': Urls(active='inventory.view') }
#
#     return render_template(template, params)


@app.route('/items/inout', methods=['GET'])
def inout():

    auth = request.authorization
    if auth:
        check_auth(auth.username, auth.password)

    if session.get('user', None) is not None:
        if request.method == 'POST':

            action = ""

            try:
                which = request.args['which']

                item = database.BackendDB.findItemByID(which)

                if 'checkout_user' in item and item['checkout_user'] is not None:
                    action = 'in'
                else:
                    action = 'out'

                if 'out' in action:
                    database.BackendDB.updateItem({'id': which}, {'checkout_user': session['user']['username'],
                                                                  'checkout_time': datetime.now().strftime("%Y/%m/%d %H:%M:%S")})
                elif 'in' in action:
                    database.BackendDB.updateItem({'id': which}, {'checkout_user': "",
                                                                  'checkout_time': ""})
                else:
                    return abort(400)

            except:
                pass

            return json.dumps({'status': 200, 'direction': 'check-'+action})

        return render_template('items_lockscreen.html', urls=sitemap.map)
    else:
        return redirect('/login?next=/inout')

@app.route('/items/inout', methods=['POST'])
def inout_post():

    if session.get('user', None) is not None:
        if request.method == 'POST':

            action = ""

            try:
                which = request.form.get('which')

                if which is None:
                    which = request.args.get('which')

                item = database.BackendDB.findItemByID(which)

                if 'checkout_user' in item and len(item['checkout_user']) > 0:
                    action = 'in'
                else:
                    action = 'out'

                print "ACTION: " + action

                if action == 'out':
                    database.BackendDB.updateItem({'id': which}, {'checkout_user': session['user']['username'],
                                                                  'checkout_time': datetime.now().strftime("%Y/%m/%d %H:%M:%S")})
                elif action == 'in':
                    database.BackendDB.updateItem({'id': which}, {'checkout_user': "",
                                                                  'checkout_time': ""})
                else:
                    return abort(400)

            except Exception as e:

                return json.dumps({'status': 400, 'message': e.message})

            print "UPDATED ITEM %s" % str(database.BackendDB.findItemByID(which))

            return json.dumps({'status': 200, 'direction': 'check-'+action})

        return render_template('items_lockscreen.html', urls=sitemap.map)
    else:
        return redirect('/login?next=/inout')

@app.route('/inout')
@requires_auth
def general_inout():

    return render_template('lockscreen.html')

def main():
    app.run()

if __name__ == '__main__':
    app.run( host='0.0.0.0', port=3000)
