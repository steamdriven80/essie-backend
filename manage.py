#!/usr/bin/env python2

import os,sys
import importlib
import cli.cmd, cli.helpers
from cli.mgr import CmdMgr

def print_usage():

    print """
ESSIE Inventory Backend CLI"
Dept. of Civil and Coastal Engineering
University of Florida

Developed 2014-2015 by Chris Pegrossi, CSE Undergraduate


    Usage: ./%s <subcommand> [OPTIONS] [ARGUMENTS]

    where <subcommand> is one of the following:""" % sys.argv[0]

    where = 10

    for module in CmdMgr.ready:
        where += len(module)

        if where > 70:
            print " "*10
            where = 10

        print module


def load(parents):

    for key in parents:

        Dir = key.keys()[0]
        pkg = key.values()[0]

        for root, dirs, files in os.walk(Dir, topdown=False):
            for name in files:
                if name.startswith('_') or not name.endswith(".py"):
                    continue
                mod = importlib.import_module(pkg+"."+name[:-3])
                if 'register' in mod.__dict__:
                    mod.register(CmdMgr)
                    print "Registered Module '%s'" % pkg+"."+name[:-3]
            for name in dirs:
                if name in parents:
                    cli.helpers.warn("located a similar set of packages: parent(%s) name(%s)" % (str(parents), name))
                Dir.append({name,pkg+"."+name})


if __name__ == "__main__":

    load([{"cli":"cli"}])

    print_usage()

    cli.dbping.dbping.run()